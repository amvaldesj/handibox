#!/usr/bin/python

"""
@summary: Module HandiboxMain: shows main window
@author: Victoria Valdes
@contact: vvaldes@alumnos.utalca.cl
@version: 6.0
@license: GNU GENERAL PUBLIC LICENSE
@date: December 2013
"""

import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gio, Gtk, GLib
from ScreenKeyboard import ScreenKeyboard
from ScreenMagnifier import ScreenMagnifier
from MouseTweak import MouseTweak
from CameraSettings import CameraSettings
from HandiboxSettings import HandiboxSettings
from Camera import Camera
import threading
from gi.repository.Gtk import Window

"""
Class HandiboxMain
@var CONFIGURATION_KEY: Access key to dconf schema
@requires: dconf schema "org.gnome.desktop.a11y.applications"
"""


class HandiboxMain(Gtk.ApplicationWindow):
    # Dconf Key
    CONFIGURATION_KEY = "org.gnome.desktop.a11y.applications"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # camera thread id.
        self.t = None
        
        self.configuration_key = Gio.Settings.new(self.CONFIGURATION_KEY)

        self.builder = Gtk.Builder()
        self.builder.add_from_file("glade/main.glade")
        self.window_main = self.builder.get_object("window_main")
        self.window_main.set_border_width(10)
        self.window_main.connect("delete-event", self.close_all)
        # set window alway on top.
        self.window_main.set_keep_above(True)
        
        # move windows to bottom-right
        screen = Window().get_screen()
        width, height = self.window_main.get_size()
        self.window_main.move(screen.width() - width, screen.height() - height)

        # First switch: Mouseteak ("Handiface")
        self.mousetweakswitch = self.builder.get_object("mousetweakswitch")
        self.mousetweakswitch.set_active(False)
        self.mousetweakswitch.connect("notify::active", self.enable_mousetweak)
        self.camera = None

        # Second switch: Screen Magnifier ("Handizoom")
        self.magnifierswitch = self.builder.get_object("magnifierswitch")
        self.magnifierswitch.set_active(False)
        self.magnifierswitch.connect("notify::active", self.screen_magnifier)

        # Third switch: Screen Keyboard ("Handikey")
        self.keyboardswitch = self.builder.get_object("keyboardswitch")
        self.keyboardswitch.set_active(False)
        self.keyboardswitch.connect("notify::active", self.screen_keyboard)

        # Settings button
        self.settingsbutton = self.builder.get_object("settingsbutton")
        self.settingsbutton.connect("clicked", self.view_settings)

        self.camera_image = self.builder.get_object("camera_image")

        self.window_main.show_all()

    def close_all(self, *args):
        mouseTweak = MouseTweak()
        mouseTweak.disable_MouseTweak()
        
        if self.t:
            self.camera.run = False
            self.t.join()
            self.camera.capture.release()
            
        Gtk.main_quit
        print("quit")

    # Enable or disable MouseTweak
    def enable_mousetweak(self, button, active):
        mouseTweak = MouseTweak()

        if button.get_active():
            self.mousetweakswitch.set_active(True)
            mouseTweak.enable_MouseTweak()
            self.camera = Camera(self.camera_image)
            self.t = threading.Thread(target=self.camera.capture_loop)
            self.t.start()
        else:
            mouseTweak.disable_MouseTweak()
            self.mousetweakswitch.set_active(False)
            handimouse = False
            self.camera.run = False
            self.t.join()
            self.camera.capture.release()
        

    # Enable or disable Screen Magnifier
    def screen_magnifier(self, button, active):
        screenMagnifier = ScreenMagnifier()
        if button.get_active():
            screenMagnifier.enable_ScreenMagnifier()
        else:
            screenMagnifier.disable_ScreenMagnifier()

    # Enable or disable Screen Keyboard
    def screen_keyboard(self, button, active):
        screenKeyboard = ScreenKeyboard()
        if button.get_active():
            screenKeyboard.enable_ScreenKeyboard()
        else:
            screenKeyboard.disable_ScreenKeyboard()

    # View Settings
    def view_settings(self, widget):
        HandiboxSettings()


class Application(Gtk.Application):
    def __repr__(self):
        return '<Application>'

    def __init__(self):
        super().__init__(flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.props.resource_base_path = "/org/gnome/Handibox"
        GLib.set_application_name(("Handibox"))

        self.handibox = None

    def _help(self, action, param):
        pass

    def _about(self, action, param):
        pass

    def build_app_menu(self):
        action_entries = [
            ('about', self._about),
            ('help', self._help)
        ]

        for action, callback in action_entries:
            simple_action = Gio.SimpleAction.new(action, None)
            simple_action.connect('activate', callback)
            self.add_action(simple_action)

    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if not self.handibox:
            camerasettings = CameraSettings()
            camerasettings.checkconfig()
            camerasettings.initcamera()
            self.handibox = HandiboxMain(application=self)
        
    def do_startup(self):
        Gtk.Application.do_startup(self)
        self.build_app_menu()

    def on_about(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self.window, modal=True)
        about_dialog.present()
    
    def on_quit(self, action, param):
        self.quit()

if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)
