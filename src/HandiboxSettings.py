#!/usr/bin/python
from gi.repository import Gio, Gtk
from CameraSettings import CameraSettings


class HandiboxSettings():
    # Dconf Key
    BASE_KEY = "org.gnome.desktop.a11y.mouse"

    def __init__(self):
        self.configuration_key = Gio.Settings.new(self.BASE_KEY)

        self.builder = Gtk.Builder()
        self.builder.add_from_file("glade/main.glade")

        self.dialog_pref = self.builder.get_object("dlg_pref")

        self.button_close = self.builder.get_object("btn_close")
        self.button_close.connect("clicked", self.button_close_clicked)

        self.dwell_time_scale = self.builder.get_object("dwell_time_scale")
        # --dwell-time=[0.2-3.0] Time to keep the pointer motionless
        self.dwell_time = self.configuration_key.get_double("dwell-time")
        self.dwell_time_scale.set_value(self.dwell_time)
        # "value-changed" emitted by the dwell time scale with the callback
        # function dwell_time_scale_moved
        self.dwell_time_scale.connect("value-changed",
                                      self.dwell_time_scale_moved)

        self.threshold_scale = self.builder.get_object("threshold_scale")
        # -t, --threshold=INT Ignore small pointer movements.
        # Range 0 - 30 pixels.
        self.threshold = self.configuration_key.get_int("dwell-threshold")
        self.threshold_scale.set_value(self.threshold)
        # "value-changed" emitted by the threshold scale with the callback
        # function threshold_scale_moved
        self.threshold_scale.connect("value-changed",
                                     self.threshold_scale_moved)

        self.reset_button = self.builder.get_object("reset_button")
        self.reset_button.connect("clicked", self.reset_configurations)

        # Camera selection
        camerasettings.get_camera_data()
        self.cameracombo = camerasettings.camera_list
        cameralist = Gtk.ListStore(str, str)

        # Append the data
        for i in range(len(self.cameracombo)):
            cameralist.append(self.cameracombo[i])

        self.selectcamera = self.builder.get_object("selectcamera")
        self.selectcamera.set_model(cameralist)
        # Cellrenderers to render the data
        renderer_pixbuf = Gtk.CellRendererPixbuf()
        renderer_text = Gtk.CellRendererText()
        self.selectcamera.pack_start(renderer_pixbuf, False)
        self.selectcamera.pack_start(renderer_text, False)
        self.selectcamera.add_attribute(renderer_text, "text", 0)
        self.selectcamera.add_attribute(renderer_pixbuf, "stock_id", 1)
        self.selectcamera.connect("changed", self.select_camera)

        # Set active camera
        self.selectcamera.set_active(int(camerasettings.read_id()))

        self.dialog_pref.show_all()

    # Any signal from the dwell time scale
    def dwell_time_scale_moved(self, event):
        dwell_time_value = self.dwell_time_scale.get_value()
        self.configuration_key.set_double("dwell-time", dwell_time_value)

    # Any signal from the threshold scale
    def threshold_scale_moved(self, event):
        threshold_value = self.threshold_scale.get_value()
        self.configuration_key.set_int("dwell-threshold", threshold_value)

    # Callback function connected to reset button
    def reset_configurations(self, button):
        self.configuration_key.reset("dwell-time")
        self.configuration_key.reset("dwell-threshold")
        self.dwell_time = self.configuration_key.get_double("dwell-time")
        self.threshold = self.configuration_key.get_int("dwell-threshold")
        # dwell_time_value = self.dwell_time_scale.set_value(self.dwell_time)
        self.dwell_time_scale.set_value(self.dwell_time)
        # threshold_value = self.threshold_scale.set_value(self.threshold)
        self.threshold_scale.set_value(self.threshold)

    def select_camera(self, combo):
        if combo.get_active() != 0:
            camera_id = str(combo.get_active())
            camera_id = int(combo.get_active()) - 1
            camerasettings.id_camera(camera_id)
        if combo.get_active() == 0:
            camera_id = str(-1)
            camerasettings.id_camera(camera_id)
        return True

    def button_close_clicked(self, btn=None):
        self.dialog_pref.destroy()


camerasettings = CameraSettings()
