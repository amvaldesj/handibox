#!/usr/bin/python

import gi
gi.require_version('Gdk', '3.0')

from gi.repository import Gdk
import config
import cv2 as cv
import threading
import numpy as np
from Process_manager import Process_manager
from Calibrations import Calibrations


class FaceTracking:
    def __init__(self, width, height):
        self.radius_circle = 15
        
        self.pman = Process_manager()
        self.calibrations = Calibrations()
        
        # Set FaceTraking constructor initial values
        # Haar classifier cascade
        xml_path = "../haarcascade/haarcascade_frontalface_default.xml"
        self.faceCascade = cv.CascadeClassifier(xml_path)
        # Pointer coordinate
        self.xx = 0
        # Pointer coordinate
        self.yy = 0
        # Coordinates for draw face frame
        self.pt1 = 0
        # Coordinates for draw face frame
        self.pt2 = 0
        self.realscreenwidth, self.realscreenheight = self.pman.get_screen()
        self.calibration_threshold = int(self.calibrations.get_threshold())

        # Current coordinares of pointer on screen
        self.Data = {"current": ((int(self.realscreenwidth)/2),
                                 (int(self.realscreenheight)/2))}
        # Update pointer coordinates
        self.lastData = self.Data
        # Old coordinates
        self.olddata = {"old": (0, 0)}
        # Update pointer coordinates
        self.lastold = self.olddata
        #
        self.width = width
        self.height = height
        
        self.virtual_screen_x = self.width/2  # Size of virtualscreen X axis
        self.virtual_screen_y = self.height/2  # Size of virtualscreen y axis

    def detectFace(self, image):
        
        # Haarcascade param
        # Minimum possible object size. Objects smaller than that are ignored.
        min_size = (30, 30)

        # Parameter specifying how much the image size is reduced at each
        # image scale.
        image_scale = 2

        # The factor by which the search window is scaled between the
        # subsequent scans, 1.2 means increasing window by 12 %
        haar_scale = 1.2

        # Parameter specifying how many neighbors each candidate rectangle
        # should have to retain it
        min_neighbors = 2

        # If it is set, the function uses Canny edge detector to reject some
        # image regions that contain too few or too much edges and thus can
        # not contain the searched object.
        haar_flags = 0

        # cv.Flip (image, image, 1) #Flip image
        image = cv.flip(image, 1)

        # Allocate the temporary images
        # gray = cv.CreateImage((image.width, image.height), 8, 1)
        # smallImage = cv.CreateImage((cv.Round(image.width / image_scale),cv.Round (image.height / image_scale)), 8 ,1)

        # Convert color input image to grayscale
        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

        # Scale input image for faster processing
        h, w, d = image.shape
        smallImage = cv.resize(gray,
                               (round(w/image_scale), round(h/image_scale)),
                               cv.INTER_LINEAR)

        # Equalize the histogram
        cv.equalizeHist(smallImage, smallImage)

        # Detect the faces
        face = self.faceCascade.detectMultiScale(smallImage,
                                                 scaleFactor=haar_scale,
                                                 minNeighbors=min_neighbors,
                                                 minSize=min_size)

        # If face are found
        if len(face) > 0:
            # ((x, y, w, h), n) = face[0]
            (x, y, w, h) = face[0]

            # The input to cv.HaarDetectObjects was resized, so scale the
            # Bounding box of each face and convert it to two CvPoints
            self.pt1 = (x * image_scale, y * image_scale)
            self.pt2 = ((x + w) * image_scale, (y + h) * image_scale)

            # Draw principal rectangle for face detection
            ###cv.rectangle(image, self.pt1, self.pt2, (255, 0, 0), 3, 8, 0)

            # self.xx = (self.pt1[0] + self.pt2[0])/2
            self.xx = int((self.pt1[0] + self.pt2[0])/2)

            # self.yy = (self.pt1[1] + self.pt2[1])/2
            self.yy = int((self.pt1[1] + self.pt2[1])/2)

            # Draw central point in face (green color)
            cv.circle(image, (self.xx, self.yy), 3, (0, 255, 0, 0), -1, 15, 0)

            # Draw neutral circle (blue color)
            cv.circle(image,
                      (int(self.width/2), int(self.height/2)),
                      self.radius_circle,
                      (255, 0, 0, 0),
                      2,
                      15,
                      0)

            # Write current pointer position
            self.Data["current"] = (self.xx, self.yy)
        
        return (image)

    def updateMousePos(self):
        pos = self.Data["current"]
        self.moveMouse(pos[0], pos[1])

    def moveMouse(self, x, y):
        """ update and move pointer """
        # Get coordinates x & y of virtual screen from thread
        # on UpdateMousePos method
        
        oldx, oldy = self.olddata["old"]
        # 0,0 neutral zone... no movement
        if (x == oldx and y == oldy):
            return
        if (x <= 0 and y <= 0):
            return
        
        # Get current pointer coordinates
        pdisplay, pscreen, px, py = self.pman.get_pointer()
        px = int(px)
        py = int(py)
        
        diff_x = abs((self.virtual_screen_x) - x)
        diff_y = abs((self.virtual_screen_y) - y)
        
        if (diff_x <= self.radius_circle and diff_y <= self.radius_circle):
            return
            
        if (self.virtual_screen_x > x):
            px = (px) - self.calibration_threshold
        else:
            px = (px) + self.calibration_threshold
            
        if (self.virtual_screen_y > y):
            py = (py) - self.calibration_threshold
        else:
            py = (py) + self.calibration_threshold
        
        self.olddata["old"] = (x, y)
        
        Gdk.Display.warp_pointer(pdisplay, pscreen, px, py)
        
        return
