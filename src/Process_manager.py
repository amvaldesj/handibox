#!/usr/bin/python

import gi
gi.require_version('Gdk', '3.0')

from gi.repository import Gdk
import os


class Process_manager:
    def get_screen(self):
        # Get display geometry
        display = Gdk.Display.get_default()
        monitor = display.get_primary_monitor()
        geometry = monitor.get_geometry()
        scale_factor = monitor.get_scale_factor()
        width = scale_factor * geometry.width
        height = scale_factor * geometry.height

        return width, height

    def get_pointer(self):
        # Get pointer position
        # FIXME
        # Deprecated since version 3.0: Use Gdk.Device.get_position() instead.
        display = Gdk.Display.get_default()
        screen = Gdk.Screen.get_default()
        display_pointer = display.get_pointer()
        x = display_pointer[1]
        y = display_pointer[2]

        return display, screen, x, y
