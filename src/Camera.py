#!/usr/bin/python

import cv2 as cv
import gi
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf
import os
from CameraSettings import CameraSettings
from FaceTracking import FaceTracking
from gi.repository import Gtk

_GDK_PIXBUF_BIT_PER_SAMPLE = 8

class Camera:
    def __init__(self, camera_image):
        self.run = True
        
        #Get camera id
        camerasettings = CameraSettings()
        
        ###self.id_device = int(camerasettings.read_id())
        self.id_device = -1
        
        # Width and heigth of Handibox capture window
        self.width = 320
        self.height = 240
        
        # Capture from selected camera
        self.capture = cv.VideoCapture(self.id_device)
        
        self.capture.set(cv.CAP_PROP_FRAME_WIDTH,self.width)
        self.capture.set(cv.CAP_PROP_FRAME_HEIGHT,self.height)
        
        self.camera_image = camera_image
        
        self.faceTraking = FaceTracking(self.width, self.height)
                
    def capture_loop(self):
        while self.run:
            # capture frame
            ret, image = self.capture.read()
            # detect face
            frame = self.faceTraking.detectFace(image)
            # updates mouse position
            self.faceTraking.updateMousePos()
            
            # prepare image to show in GtkImage.
            cvimage = cv.cvtColor(frame, cv.COLOR_RGB2BGR)
            data = cvimage.tostring()
            colorspace = GdkPixbuf.Colorspace.RGB
            has_alpha_channel = False
            width = cvimage.shape[1]
            height = cvimage.shape[0]
            row_stride = cvimage.strides[0]
            destroy_fn = None
            destroy_fn_data = None
            
            pix = GdkPixbuf.Pixbuf.new_from_data(
                data,
                colorspace,
                has_alpha_channel,
                _GDK_PIXBUF_BIT_PER_SAMPLE,
                width,
                height,
                row_stride,
                destroy_fn,
                destroy_fn_data,)
                
            self.camera_image.set_from_pixbuf(pix)  
